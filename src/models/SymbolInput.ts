import { SymbolType } from "./Symbol";

export declare type SymbolInput = {
  symbolType: SymbolType;
  currentLevel: number;
  currentExp: number;
  additionSymbol: number;
}