export declare type Symbol = {
  type: SymbolType;
  displayName: string;
  imgPath: string;
  requireExp: number[],
  requireMesos: number[],
}

let exps = [
  0,
  12,
  15,
  20,
  27,
  36,
  47,
  60,
  75,
  92,
  111,
  132,
  155,
  180,
  207,
  236,
  267,
  300,
  335,
  372,
]

let yeoroMesos = [
  0,
  9_500_000,
  16_630_000,
  23_760_000,
  30_890_000,
  38_020_000,
  45_150_000,
  52_280_000,
  59_410_000,
  66_540_000,
  73_670_000,
  80_800_000,
  87_930_000,
  95_060_000,
  102_190_000,
  109_320_000,
  116_450_000,
  123_580_000,
  130_710_000,
  137_840_000,
]

let othersMesos = [
  0,
  19_040_000,
  25_640_000,
  32_240_000,
  38_840_000,
  45_440_000,
  52_040_000,
  58_640_000,
  65_240_000,
  71_840_000,
  78_440_000,
  85_040_000,
  91_640_000,
  98_240_000,
  104_840_000,
  111_440_000,
  118_040_000,
  124_640_000,
  131_240_000,
  137_840_000,
]

export declare type SymbolType = 'YEORO' | 'CHUCHU' | 'LACHELEIN' | 'ARCANA' | 'MORAS' | 'ESFERA'
export let SYMBOLS: { [type: string] : Symbol } = {
  YEORO: {
    type: 'YEORO',
    displayName: "소멸의 여로",
    imgPath: '/yeoro.png',
    requireExp: exps,
    requireMesos: yeoroMesos,
  },
  CHUCHU: {
    type: 'CHUCHU',
    displayName: "츄츄 아일랜드",
    imgPath: "/chuchu.png",
    requireExp: exps,
    requireMesos: othersMesos,
  },
  LACHELEIN: {
    type: 'LACHELEIN',
    displayName: "레헬른",
    imgPath: "/lachelein.png",
    requireExp: exps,
    requireMesos: othersMesos,
  },
  ARCANA : {
    type: 'ARCANA',
    displayName: "아르카나",
    imgPath: "/arcana.png",
    requireExp: exps,
    requireMesos: othersMesos,
  },
  MORAS : {
    type: 'MORAS',
    displayName: "모라스",
    imgPath: "/moras.png",
    requireExp: exps,
    requireMesos: othersMesos,
  },
  ESFERA : { 
    type: 'ESFERA',
    displayName: "에스페라",
    imgPath: "/esfera.png",
    requireExp: exps,
    requireMesos: othersMesos,
  },
}