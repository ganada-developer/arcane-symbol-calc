import React from 'react';
import '@kfonts/nanum-square'
import { Container } from '@material-ui/core'
import { unstable_createMuiStrictModeTheme as createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import './App.css';
import Header from './elements/Header'
import Content from './elements/Content'
import Footer from './elements/Footer'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ec407a'
    },
    secondary: {
      main: '#f06292'
    },
  },
  typography: {
    fontFamily: 'nanum-square',
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <Container maxWidth="sm" fixed>
          <Header />
          <Content />
          <Footer />
        </Container>
      </React.Fragment>
    </ThemeProvider>
  );
}

export default App;
