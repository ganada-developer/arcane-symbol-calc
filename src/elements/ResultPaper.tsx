import { Grid, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import SymbolResult from './SymbolResult'

const useStyles = makeStyles({
  root: {
    backgroundColor: '#ffffff',
    color: '#9ea7aa',
    padding: '3px'
  },
});

export type Result = {
  symbolLevel: number;
  symbolExp: number;
  totalExp: number;
  neededMesos: number;
}

type ResultProps = {
  symbols: { [type: string] : Result }
}

function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default function ResultPaper(props: ResultProps) {
  const classes = useStyles();

  let totalMesos = 0;
  Object.keys(props.symbols).forEach((key) => {
    totalMesos += props.symbols[key].neededMesos;
  });

  return (
      <Grid container className={classes.root}>
        <Grid container>
          <SymbolResult type='YEORO' result={props.symbols['YEORO']} />
          <SymbolResult type='CHUCHU' result={props.symbols['CHUCHU']} />
          <SymbolResult type='LACHELEIN' result={props.symbols['LACHELEIN']} />
          <SymbolResult type='ARCANA' result={props.symbols['ARCANA']} />
          <SymbolResult type='MORAS' result={props.symbols['MORAS']} />
          <SymbolResult type='ESFERA' result={props.symbols['ESFERA']} />
        </Grid>
        <Grid item>
          <Typography variant="caption" color='textPrimary'>총 필요 메소</Typography>
          <Typography variant="h4" color='textPrimary'>{numberWithCommas(totalMesos)}</Typography>
        </Grid>
      </Grid>
  );
};
