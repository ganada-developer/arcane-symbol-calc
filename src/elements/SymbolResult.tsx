import { Card, CardContent, CardMedia, Typography, Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { SymbolType, SYMBOLS } from '../models/Symbol'
import { Result } from './ResultPaper';

const useStyles = makeStyles({
  root: {
    display: 'inline-block',
    padding: '5px',
    maring: '5px',
  },
  title: {
    fontSize: 15,
  },
  image: {
    height: '48px',
    backgroundSize: '48px',
  },
  textField: {
    margin: '3px',
  },
});

type SymbolResultProps = {
  type: SymbolType;
  result: Result;
}

function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default function SymbolSelector(props: SymbolResultProps) {
  const classes = useStyles();
  let symbol = SYMBOLS[props.type]
  return (
    <Grid className={classes.root} item xs={6} sm={4}>
      <Card>
        <CardContent>
          <Typography className={classes.title} color="primary" gutterBottom align='center'>
            {symbol.displayName}
          </Typography>
          <CardMedia
            className={classes.image}
            image={symbol.imgPath}
            title={symbol.displayName}
          />
          <Typography variant="caption">최종 심볼 레벨</Typography>
          <Typography variant="h6">{numberWithCommas(props.result?.symbolLevel || 0)}</Typography>
          <Typography variant="caption">최종 심볼 EXP</Typography>
          <Typography variant="h6">{numberWithCommas(props.result?.symbolExp || 0)} / {numberWithCommas(props.result?.totalExp || 0)}</Typography>
          <div style={{width: '100%', backgroundColor: '#ffffff', height: '20px'}} />
          <Typography variant="caption">필요 메소</Typography>
          <Typography variant="h6">{numberWithCommas(props.result?.neededMesos || 0)}</Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};