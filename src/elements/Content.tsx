import SymbolSelector, { FieldType } from './SymbolSelector';
import ResultPaper, { Result } from './ResultPaper';
import { Button, Grid, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useState } from 'react';
import { SymbolInput } from '../models/SymbolInput';
import { Symbol, SYMBOLS } from '../models/Symbol'

const useStyles = makeStyles({
  root: {
    backgroundColor: '#ffffff',
    flexGrow: 1,
    padding: '10px'
  },
  submit: {
    margin: '10px 0px',
  }
});


function validateBefore(symbol: Symbol, userInput: SymbolInput) {
  if (userInput.currentLevel < 0 || userInput.currentLevel > 20) {
    throw new Error(`'${symbol.displayName}' 심볼 레벨을 0 ~ 20 사이로 설정해주세요.`);
  }
  if (userInput.currentExp < 0) {
    throw new Error(`'${symbol.displayName}' 심볼 경험치는 음수값이 될 수 없습니다.`);
  }
  if (userInput.currentLevel === 0 && userInput.currentExp !== 0) {
    throw new Error(`'${symbol.displayName}' 심볼 경험치를 0으로 설정해주세요.\n(현재 레벨 : ${userInput.currentLevel}, 경험치 : ${userInput.currentExp})`);
  }
  if (symbol.requireExp[userInput.currentLevel] < userInput.currentExp) {
    throw new Error(`'${symbol.displayName}' 심볼 레벨 ${userInput.currentLevel}의 경험치는 ${symbol.requireExp[userInput.currentLevel] + 1} 이상일 수 없습니다.\n(현재 : ${userInput.currentExp})`);
  }
  if (userInput.currentLevel === 20 && userInput.currentExp > 0) {
    throw new Error(`'${symbol.displayName}' 심볼은 이미 만렙입니다.`)
  }
  if (userInput.additionSymbol < 0) {
    throw new Error(`'${symbol.displayName}' 심볼에 사용할 심볼은 음수값이 될 수 없습니다.`)
  }
}

function calculateResult(symbols: {[type: string] : SymbolInput}): {[type: string] : Result} {
  var results: {[type: string] : Result} = {}
  Object.keys(symbols).forEach((key) => {
    let symbol = SYMBOLS[key];
    let userSymbol = symbols[key];
    validateBefore(symbol, userSymbol);

    var additionSymbol: number = userSymbol.additionSymbol;
    var exp: number = userSymbol.currentExp;
    var level: number = userSymbol.currentLevel
    var mesos: number = 0;
    while (additionSymbol >= (symbol.requireExp[level] - exp)) {
      additionSymbol = additionSymbol - (symbol.requireExp[level] - exp);
      mesos += symbol.requireMesos[level];
      exp = 0;
      level += 1;
      if (level === 20 && additionSymbol > 0) {
        throw new Error(`'${symbol.displayName}' 심볼 만렙을 위해선 '${userSymbol.additionSymbol - additionSymbol}'개로 충분합니다.\n '${additionSymbol}'개는 다른데에 투자해주세요.`)
      }
    }

    let result: Result = {
      symbolLevel: (userSymbol.currentLevel === 0) ? 0 : level,
      symbolExp: exp + additionSymbol,
      totalExp: (userSymbol.currentLevel === 0) ? 0 : symbol.requireExp[level],
      neededMesos: mesos,
    };
    results[key] = result;
  });

  return results
}

function onChangeValue(symbols: {[type: string]: SymbolInput}, type: Symbol, field: FieldType, value: number) {
  if (symbols[type.type] === undefined) {
    symbols[type.type] = {
      symbolType: type.type,
      currentLevel: 0,
      currentExp: 0,
      additionSymbol: 0,
    }
  }

  switch (field) {
    case 'current-symbol-level':
      symbols[type.type].currentLevel = value
      break;
    case 'current-symbol-exp':
      symbols[type.type].currentExp = value
      break;
    case 'addition-symbol':
      symbols[type.type].additionSymbol = value
      break;
  }
  return symbols
}


export default function Content() {
  const classes = useStyles();

  const [hidden, setHidden] = useState(true);
  // eslint-disable-next-line
  const [symbols, _] = useState({});
  const [result, setResult] = useState({});

  return (
    <Container className={classes.root}>
      <Grid container>
        <SymbolSelector type='YEORO' callback={(type: Symbol, field: FieldType, value: number) => onChangeValue(symbols, type, field, value)} />
        <SymbolSelector type='CHUCHU' callback={(type: Symbol, field: FieldType, value: number) => onChangeValue(symbols, type, field, value)} />
        <SymbolSelector type='LACHELEIN' callback={(type: Symbol, field: FieldType, value: number) => onChangeValue(symbols, type, field, value)} />
        <SymbolSelector type='ARCANA' callback={(type: Symbol, field: FieldType, value: number) => onChangeValue(symbols, type, field, value)} />
        <SymbolSelector type='MORAS' callback={(type: Symbol, field: FieldType, value: number) => onChangeValue(symbols, type, field, value)} />
        <SymbolSelector type='ESFERA' callback={(type: Symbol, field: FieldType, value: number) => onChangeValue(symbols, type, field, value)} />
      </Grid>
      <Button
        className={classes.submit}
        variant='contained'
        color='primary'
        onClick={() => {
          try {
            let newResult = calculateResult(symbols);
            setResult(newResult);
            setHidden(false);
          } catch (e) {
            alert(e);
          }
        }}
        fullWidth
      >
        계산하기
      </Button>
      <div style={hidden ? {display: 'none'} : {}}>
      <ResultPaper symbols={result} />
      </div>
    </Container>
  );
};
