import { Card, CardContent, CardMedia, Typography, Grid, TextField } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import { SymbolType, SYMBOLS, Symbol } from '../models/Symbol'

const useStyles = makeStyles({
  root: {
    display: 'inline-block',
    padding: '5px',
    maring: '5px',
  },
  title: {
    fontSize: 15,
  },
  image: {
    height: '48px',
    backgroundSize: '48px',
  },
  textField: {
    margin: '3px',
  },
});

type SymbolSelectorProps = {
  type: SymbolType;
  callback: (type: Symbol, field: FieldType, value: number) => void;
}

export declare type FieldType = 'current-symbol-level' | 'current-symbol-exp' | 'addition-symbol'

export default function SymbolSelector(props: SymbolSelectorProps) {
  const classes = useStyles();

  let callback = props.callback
  let symbol = SYMBOLS[props.type]
  return (
    <Grid className={classes.root} item xs={6} sm={4}>
      <Card>
        <CardContent>
          <Typography className={classes.title} color="primary" gutterBottom align='center'>
            {symbol.displayName}
          </Typography>
          <CardMedia
            className={classes.image}
            image={symbol.imgPath}
            title={symbol.displayName}
          />
          <TextField 
            className={classes.textField}
            id='current-symbol-level'
            label='현재 심볼 레벨'
            type='number'
            placeholder='0'
            InputLabelProps={{shrink: true}}
            onChange={(event) => callback(symbol, 'current-symbol-level', +event.target.value)}
            fullWidth
          />
          <TextField 
            className={classes.textField}
            id='current-symbol-exp'
            label='현재 심볼 EXP'
            type='number'
            placeholder='0'
            onChange={(event) => callback(symbol, 'current-symbol-exp', +event.target.value)}
            InputLabelProps={{shrink: true}}
            fullWidth
          />
          <div style={{width: '100%', backgroundColor: '#ffffff', height: '20px'}} />
          <TextField 
            className={classes.textField}
            id='addition-symbol'
            label='사용할 심볼'
            color='secondary'
            type='number'
            placeholder='0'
            onChange={(event) => callback(symbol, 'addition-symbol', +event.target.value)}
            InputLabelProps={{shrink: true}}
            fullWidth
          />
        </CardContent>
      </Card>
    </Grid>
  );
};