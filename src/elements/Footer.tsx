import { Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    backgroundColor: '#ffffff',
    color: '#9ea7aa',
    padding: '3px'
  },
});

export default function Footer() {
  const classes = useStyles();

  return (
    <Typography align="center" variant="subtitle2" className={classes.root}>
        Copyright © 2021 가나다
    </Typography>
  );
};
