import { AppBar, Typography, Toolbar } from '@material-ui/core'

export default function Header() {
  return (
    <AppBar position="static" color="primary">
        <Toolbar>
            <Typography variant="h6">
                아케인 심볼 등급업 메소 계산기
            </Typography>
            &nbsp;
            <Typography variant="caption">
                by 가나다
            </Typography>
        </Toolbar>
    </AppBar>
  );
};
